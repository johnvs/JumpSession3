/* ------------------------------------ 
Click on login and Sign Up to changue and view the effect
---------------------------------------
*/



/* *********************************************************************************************** */
// FUNCTION FOR THE LOGIN ANIMATION
/* *********************************************************************************************** */
function cambiar_login() {
  document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_login";
  document.querySelector('.cont_form_login').style.display = "block";
  document.querySelector('.cont_form_sign_up').style.opacity = "0";

  setTimeout(function () { document.querySelector('.cont_form_login').style.opacity = "1"; }, 400);

  setTimeout(function () {
    document.querySelector('.cont_form_sign_up').style.display = "none";
  }, 200);
}
/* *********************************************************************************************** */


/* *********************************************************************************************** */
// FUNCTION FOR THE SIGN UP ANIMATION
/* *********************************************************************************************** */
function cambiar_sign_up(at) {

  document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_sign_up";
  document.querySelector('.cont_form_sign_up').style.display = "block";
  document.querySelector('.cont_form_login').style.opacity = "0";

  setTimeout(function () {
    document.querySelector('.cont_form_sign_up').style.opacity = "1";
  }, 100);

  setTimeout(function () {
    document.querySelector('.cont_form_login').style.display = "none";
  }, 400);
}
/* *********************************************************************************************** */



function ocultar_login_sign_up() {

  document.querySelector('.cont_forms').className = "cont_forms";
  document.querySelector('.cont_form_sign_up').style.opacity = "0";
  document.querySelector('.cont_form_login').style.opacity = "0";

  setTimeout(function () {
    document.querySelector('.cont_form_sign_up').style.display = "none";
    document.querySelector('.cont_form_login').style.display = "none";
  }, 500);

}

function logInRequest(data) {
 
  var username = document.getElementById("LoginUsername").value;
  var password = document.getElementById("LoginPassword").value;

}

function signUpRequest(data) {
  var password = document.getElementById("sign_Pass").value;
  var passwordConfirm = document.getElementById("sign_PassCon").value;
  var username = document.getElementById("sign_User").value;
  var email = document.getElementById("sign_Email").value;
}

  //https://scotch.io/tutorials/how-to-use-the-javascript-fetch-api-to-get-data


/*
----------------------------------------
POST Request

It's not uncommon for web apps to want to call an API with a POST method and supply some parameters in the body of the request.

To do this we can set the method and body parameters in the fetch() options.

fetch(url, {
    method: 'post',
    headers: {
      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
    },
    body: 'foo=bar&lorem=ipsum'
  })
  .then(json)
  .then(function (data) {
    console.log('Request succeeded with JSON response', data);
  })
  .catch(function (error) {
    console.log('Request failed', error);
  });

----------------------------------------
*/


/*
----------------------------------------
FETCH

Our fetch request looks a little like this:

fetch('./api/some.json')
  .then(
    function(response) {
      if (response.status !== 200) {
        console.log('Looks like there was a problem. Status Code: ' +
          response.status);
        return;
      }

      // Examine the text in the response
      response.json().then(function(data) {
        console.log(data);
      });
    }
  )
  .catch(function(err) {
    console.log('Fetch Error :-S', err);
  });

We start by checking that the response status is 200 before parsing the response as JSON.

The response of a fetch() request is a Stream object, which means that when we call the json() method, a Promise is returned since the reading of the stream will happen asynchronously.
----------------------------------------
*/